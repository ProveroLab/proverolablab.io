## People
<style>
.fig {
padding: 10px;
}
</style>
<img src="/img/paolo.jpg" height="160" align="left" alt="avatar" class=fig> __Paolo Provero__  
*PI, Full professor*  
I am interested in understanding the role of gene regulation in human evolution, complex traits, and diseases using statistics, data mining, and machine learning. *Profiles:*  [Scopus](https://www.scopus.com/authid/detail.uri?authorId=55840319900), [Google Scholar](https://scholar.google.com/citations?user=0cO2nlMAAAAJ&hl=en), [Linkedin](https://www.linkedin.com/in/paolo-provero/)
<br clear="left"/>

<img src="/img/davide.jpg" height="160" align="left" alt="avatar" class=fig> __Davide Marnetto__  
*Research fellow (RTDa)*  
I am a computational biologist always keen on exploring novel territory. So far, I have been investigating human evolution, variation, and their effects, from gene regulation to complex traits. Tolkien addict. *Profiles:* [ResearchGate](https://www.researchgate.net/profile/Davide-Marnetto), [Scopus](https://www.scopus.com/authid/detail.uri?authorId=56257658000), [Linkedin](https://www.linkedin.com/in/davide-marnetto-498b74172/)
<br clear="left"/>

<img src="/img/mathilde.jpg" height="160" align="left" alt="avatar"  class=fig> __Mathilde André__  
*Postdoc researcher*  
I am a postdoctoral researcher with a background in Population Genomics. My current research focuses on unraveling the role of gene transcription in complex traits. My other interests also include exploring how natural selection has shaped the phenotypic diversity observed in modern populations. *Profiles:* [Twitter](https://twitter.com/MathildeAndr11), [ResearchGate](https://www.researchgate.net/profile/Mathilde-Andre-4), [Linkedin](https://www.linkedin.com/in/mathilde-andr%C3%A9-93b870238/)
<br clear="left"/>

<img src="/img/giorgia.jpg" height="160" align="left" alt="avatar"  class=fig> __Giorgia Modenini__  
*Postdoc researcher*  
I am a postdoc in Evolutionary Anthropology/Bioinformatics. I am interested in the role of Transposable Elements (TEs) in human evolution and diseases. In particular, I study the functional influence of TEs on genes' regulation. *Profiles:* [ResearchGate](https://www.researchgate.net/profile/Giorgia-Modenini), [Scopus](https://www.scopus.com/authid/detail.uri?authorId=57223241813)
<br clear="left"/>

<img src="/img/avatar.png" height="160" align="left" alt="avatar"  class=fig> __Ettore Zapparoli__  
*Postdoc researcher*  
<br clear="left"/>

<img src="/img/avatar.png" height="160" align="left" alt="avatar"  class=fig> __Alessandro Amaolo__  
*Postdoc researcher*  
<br clear="left"/>

<img src="/img/daniela.jpg" height="160" align="left" alt="avatar"  class=fig> __Daniela Fusco__  
*PhD student*  
I am a PhD student in Artificial Intelligence for health and life sciences ([National Phd in AI](https://www.phd-ai.it), XXXVIII cycle) with a background in Molecular Biology.
<br clear="left"/>

<img src="/img/avatar.png" height="160" align="left" alt="avatar"  class=fig> __Camilla Marinelli__  
*PhD student*  
<br clear="left"/>

<img src="/img/yari.jpg" height="160" align="left" alt="avatar"  class=fig> __Yari Cerruti__  
*Research assistant*  
I am interested in natural selection and its effects on genome evolution. I love making plots and taking pictures. *Profiles:* [Twitter](https://twitter.com/yaricerruti), [ResearchGate](https://www.researchgate.net/profile/Yari-Cerruti), [Linkedin](https://www.linkedin.com/in/yari-cerruti-94014023b/)
<br clear="left"/>

<img src="/img/avatar.png" height="160" align="left" alt="avatar"  class=fig> __Mahdis Jahanbin__  
*Master's student*  
<br clear="left"/>

<img src="/img/avatar.png" height="160" align="left" alt="avatar"  class=fig> __Francesco Castano__  
*Master's student*  
<br clear="left"/>

<!---
<img src="/img/avatar.png" height="150" align="left" alt="avatar"  class=fig> __Somebody__  
*role*  
something, [link](https://duckduckgo.com/)
<br clear="left"/>
--->

### Visiting Researchers
<img src="/img/avatar.png" height="160" align="left" alt="avatar"  class=fig> __Roberta Zeloni__  
*Research assistant*  
<br clear="left"/>

<img src="/img/avatar.png" height="160" align="left" alt="avatar"  class=fig> __Irene Treccani__  
*PhD student*
<br clear="left"/>

### Collaborators
- Ferdinando Di Cunto
- Elena Grassi
- Luca Pagani
- Fabrizio Pizzagalli

### Former Members 
- Ugo Ala
- Simona Baghai
- Marika Catapano
- Christian Damasco
- Rachele D'Angelo
- Mattia Forneris
- Stefano Gilotto
- Elena Grassi
- Antonio Lembo
- Alessandro Lussana
- Federica Mantica
- Elisa Mariella
- Federico Marotta
- Ivan Molineris
- Reza Mozafari
- Monica Nicolau
- Umberto Perron
- Eva Pinatel
- Rosario Piro
- Hélène Tonnelé
- Lucia Troiani
- Giacomo Turrini
