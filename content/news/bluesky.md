---
title: Hello Bluesky!
subtitle: Follow @cbuunito.bsky.social
date: 2024-11-27
tags: ["bluesky","social"]
---
In the last couple of weeks, we first glanced at, then tried out, and finally dove into Bluesky Social, where a great scientific community is rapidly building up. We are therefore announcing that we will use our [Bluesky profile](https://bsky.app/profile/cbuunito.bsky.social) to share announcements, news, and cool papers. Meanwhile, our Twitter (now X) account will enter a *"holiday. A very long holiday. And we don't expect we shall return. In fact we mean not to."*

Hello Bluesky!
