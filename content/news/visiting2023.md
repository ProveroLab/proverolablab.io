---
title: Visiting Researcher Fellowships
subtitle: Opportunities for a short-term research stay in Turin
date: 2023-03-29
tags: ["positions","visiting"]
---
Are you a young computational biology researcher looking for a change of perspective or willing to establish an international collaborative network? We might have the opportunity for you!

We are offering short-term visiting researcher positions of three months between May and December 2023 at our computational biology laboratory within the [Dept. of Neurosciences of the University of Turin](https://neuroen.campusnet.unito.it/do/home.pl), Italy.

The goal of the visiting period is to establish a collaboration on a project within the scope of the [laboratory interests](https://proverolab.gitlab.io/research/). These broadly include: predictive genomics, evolutionary genetics, imaging genetics, transcriptomics including single cell transcriptomics. The specific project will be discussed beforehand and can be proposed by the applicant within the scope above, or suggested by us in accord with the applicant's interests.

Visiting Researchers will receive a reimbursement fellowship that will cover a three-month stay in Turin at a fixed rate of 1000 Euros, plus traveling expenses. The reimbursement will be funded through UniTo Grants for Internationalization.

To be eligible, applicants should be graduate researchers with a background in computational and/or biological sciences, currently affiliated with an institution abroad.

If interested please get in contact with us by email ([paolo.provero@unito.it](paolo.provero@unito.it); [davide.marnetto@unito.it](davide.marnetto@unito.it)) enclosing a CV and either a project abstract (200 words) or stating the scientific interests that you would like to pursue in your stay!
