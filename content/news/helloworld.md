---
title: Hello world!
subtitle: We are online
date: 2022-02-28
tags: ["news","online"]
---

This is our first post, follow us here and on [Twitter](https://twitter.com/CBUuniTo) to be up to date with our projects, publications and open positions!
