---
title: Postdoc position in Statistical Genetics at the University of Turin
subtitle: Send us your expression of interest and CV by October 15th!
date: 2023-09-15
tags: ["positions","statistical genetics","assortative mating"]
---
We will have one postdoc position at the university of Torino, starting in January 2024, as part of a larger collaboration with the Universities of Padova and Bologna.

The position is aimed at young researchers with a background in computational and/or biological sciences, to work in a project at the crossroads between population genomics and complex trait analysis. Please see full details [here](https://www.anthropopgen.it/calls-for-interest-jan-2024).

Send your expression of interest including a CV at [davide.marnetto@unito.it](davide.marnetto@unito.it) by October 15th 2023!
