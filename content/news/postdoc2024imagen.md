---
title: Postdoc position at the University of Turin
subtitle: Computational research at the crossroads of Neuroimaging, Genomics and Transcriptomics
date: 2024-06-10
tags: ["positions","genomics","imaging genetics","transcriptomics"]
---
We will have a position starting from **November 2024** for a young postdoctoral researcher. The main project on which we are funding the position is aimed at investigating the impact of Neanderthal-derived (and in general recently evolved) genetic variants on traits extracted from Brain Magnetic Resonance Imaging (bMRI). The project is in collaboration with Luca Pagani Lab at the University of Padova.

Depending on the interests of the candidate, this problem can be tackled from a combination of different angles:
- The Genomic aspect, supervised by Davide Marnetto will investigate the  genetic basis of the traits of interest, with a focus on evolutionary and variation patterns.  
- The Neuroimaging aspect, supervised by Fabrizio Pizzagalli, will focus on feature extraction methods applied to bMRI scans from public international datasets such as the UK Biobank and the Adolescent Brain Cognitive Development study.  
- The Transcriptomic aspect, supervised by Paolo Provero will analyze the molecular determinants of these complex traits, leveraging on RNA-seq and ATAC-seq from bulk and single-cell experiments.  

**What we are offering**  
A position within the Computational Biology and NeuroImaging Genetics labs at the Dept. of Neuroscience, Corso Massimo D'Azeglio 52, 10126, Turin, Italy.
Details:
- **Contract duration:** 12 months starting from November 2024 + potential extension  
- **Net monthly salary:** approx. 1850 EUR/month after all income and local taxes  
- **Contract type:** Assegno di Ricerca  

**Who we are**  
The Computational Biology Unit is a medium-small purely computational group led by Prof. Paolo Provero together with Davide Marnetto, with decades of experience in bioinformatics and computational genomics, in addition to a great track record in mentoring young researchers towards expertise in computational biology research.

The NeuroImaging Genetics Lab, led by Prof. Pizzagalli, is dedicated to unraveling the intricate links between genetic factors and human brain anatomy and functions. Prof. Pizzagalli, experienced in computational neuroanatomy and neuroimaging data analysis, leads the ENIGMA-SULCI group and collaborates internationally.

**Who we are looking for**  
Young post-docs or post-lauream researchers (a recent PhD will be considered a plus but is not required) interested in any of these aspects. Previous experience in Linux command line environment, analysis of large data sets and in any of the fields above will be considered a significant plus. We will also consider less experienced candidates seriously committed to transitioning towards these fields.

If interested please get in contact with us **before July 15th 2024** by e-mail ([davide.marnetto@unito.it](davide.marnetto@unito.it); [paolo.provero@unito.it](paolo.provero@unito.it); [fabrizio.pizzagalli@unito.it](fabrizio.pizzagalli@unito.it)) enclosing a CV!
