---
title: A calm summer break before a (brain)stormy new year!
subtitle: Three PRINs at our lab starting next fall semester.
date: 2023-07-28
tags: ["positions","projects","assortative mating","imaging genetics","neandertal","cortical folding"]
---
<img src="/img/poolpug.jpg" height="300" align="right" alt="Furry analogy of our spirits in this moment" style="padding:10px" title="Furry analogy of our spirits in this moment">

**Three** PRINs landed in our lab this last couple of months, making us very honored and proud as we enter our summer break. For our international friends, **PRIN** (Progetti di Ricerca di Interesse Nazionale) are among the most important national funding programs in Italy, designed to fund large collaborative projects. As a group, in the next couple of years, we will be part of two PRINs 2022 and leading the investigation in a PRIN PNRR 2022.

In one project, Paolo and our collaborator Fabrizio Pizzagalli will join efforts with the University of Verona to unravel the genetic mechanisms underlying complex traits with a neurological and behavioral basis, combining transcriptional and Magnetic Resonance Imaging data as intermediate traits. Our collaborators will analyze brain MRI scans in multiple modalities with machine learning techniques to extract morphological and functional quantitative traits, that we will add to genetic and transcriptional variables to build integrative models of the convolute path from genotype to phenotype.

The second project, [ARGONAUT](https://www.anthropopgen.it/argonaut), will involve Davide in collaboration with Pagani and Barban groups at Padova and Bologna universities. Here we aim to detect genetic assortative mating (GAM) for anthropometric, behavioral, cognitive and health-related complex traits in up to 600k individuals from the UK Biobank and the Estonian Biobank. We will analyze the similarity between parental haploid genomes, apply state-of-the-art methods to identify parental haploid genomes and compute parent-specific polygenic scores.

The third project, UMFOLD, will be spearheaded by Davide in collaboration with UniTo's Fabrizio Pizzagalli and UniPd's Luca Pagani. The project aims to identify the most recent evolutionary innovations that define the anatomy of the modern human brain and to connect them with their consequences on mental health. Leveraging genetic variants derived from Neandertal introgression recorded in large biobanks that combine genetic and brain MRI data, we will explore the archaic hominin contribution to the genetic basis of brain MRI features, with a focus on cortical sulci.

This indeed means that if you are interested to join our efforts, it will be a fall semester full of opportunities! At least two calls for postdocs will be issued, so keep an eye out and if you wanna know more feel encouraged to contact us at [paolo.provero@unito.it](paolo.provero@unito.it) and [davide.marnetto@unito.it](davide.marnetto@unito.it)!
