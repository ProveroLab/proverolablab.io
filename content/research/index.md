## Research

### Genetic basis of gene expression
<img src="/img/trex.png" height="350" align="left" alt="Figure from Marotta et al. 2021" style="padding:10px">

We are interested in how genetic variation shapes the regulation of gene expression and other molecular phenotypes. Our current focus is the prediction of gene expression from sequence and chromatin data in human cell lines. Our approach is based on Total Binding Affinity as a predictor of the binding of transcription factors to cis-regulatory regions. This approach naturally takes into account the contribution of weak binding sites and, from an evolutionary perspective, the phenomenon of binding site turnover.
<font size="2">

__Relevant publications:__
- Marotta F, Mozafari R, Grassi E, Lussana A, Mariella E, Provero P. Prediction of gene expression from regulatory sequence composition enhances transcriptome-wide association studies. Preprint. bioRxiv. 2021. doi: [10.1101/2021.05.11.443571](https://doi.org/10.1101/2021.05.11.443571)
- Grassi E, Mariella E, Forneris M, Marotta F, Catapano M, Molineris I, Provero P. A functional strategy to characterize expression Quantitative Trait Loci. Hum Genet. 2017 Nov;136(11-12):1477-1487. doi: [10.1007/s00439-017-1849-9](https://doi.org/10.1007/s00439-017-1849-9)

</font>
<br clear="left"/>

### Evolution of gene expression
<img src="/img/gage.png" height="400" align="left" alt="Figure from Marnetto et al. 2018" style="padding:10px">

We are interested in the evolution of regulatory regions as a major determinant of anatomical and behavioral differences in humans, great apes and ultimately vertebrates. Our focus is on the role of genome expansion mediated by transposable elements in rewiring the gene regulatory networks through the generation of transcription factor binding sites.
<font size="2">

__Relevant publications:__
- Marnetto D, Mantica F, Molineris I, Grassi E, Pesando I, Provero P. Evolutionary Rewiring of Human Regulatory Networks by Waves of Genome Expansion. Am J Hum Genet. 2018 Feb 1;102(2):207-218. doi: [10.1016/j.ajhg.2017.12.014](https://doi.org/10.1016/j.ajhg.2017.12.014)
- Marnetto D, Molineris I, Grassi E, Provero P. Genome-wide identification and characterization of fixed human-specific regulatory regions. Am J Hum Genet. 2014 Jul 3;95(1):39-48. doi: [10.1016/j.ajhg.2014.05.011](https://doi.org/10.1016/j.ajhg.2014.05.011)

</font>
<br clear="left"/>

### Post-transcriptional regulation
<img src="/img/roar.png" height="200" align="left" alt="Figure from Grassi et al. 2016" style="padding:10px">
We investigate the role of 3' UTRs in the post-transcriptional regulation of gene expression, in two related aspects: (a) competing endogenous RNAs, where the titration of microRNA abundance by their targets results in a regulatory cross-talk by targets of the same microRNAs that is wholly independent of their gene products (b) shortening of 3' UTR, in which proliferating cells escape regulation by microRNAs and other RNA-binding trans-acting factors by expressing shortened 3' UTR isoforms lacking binding sites for those factors.
<font size="2">

**Relevant publications:**
- Mariella E, Marotta F, Grassi E, Gilotto S, Provero P. The Length of the Expressed 3' UTR Is an Intermediate Molecular Phenotype Linking Genetic Variants to Complex Diseases. Front Genet. 2019 Aug 16;10:714. doi: [10.3389/fgene.2019.00714](https://doi.org/10.3389/fgene.2019.00714)
- Grassi E, Mariella E, Lembo A, Molineris I, Provero P. Roar: detecting alternative polyadenylation with standard mRNA sequencing libraries. BMC Bioinformatics. 2016 Oct 18;17(1):423. doi: [10.1186/s12859-016-1254-8](https://doi.org/10.1186/s12859-016-1254-8)

</font>
<br clear="left"/>

### Variation across human populations and functional implications
<img src="/img/anceurcontrib.png" height="300" align="left" alt="Figure from Marnetto et al. 2022" style="padding:10px">
Another interest of our laboratory is the analysis of human variation, for which we developed bioinformatic tools and  pipelines. We are currently focused on how genetic variation shapes the complex trait landscape within and across human populations and on the development of approaches to overcome the inherent population-dependency of complex trait predictive methods.
<font size="2">

**Relevant publications:**
- Marnetto D, Pankratov V, Mondal M, Montinaro F, Pärna K, Vallini L, Molinaro L, Saag L, Loog L, Montagnese S, CostaR , Estonian Biobank Research Team, Metspalu M, Eriksson A, Pagani L. Ancestral genomic contributions to complex traits in contemporary Europeans. Curr Biol. 2022 Feb 1:S0960-9822(22)00108-7. doi: [10.1016/j.cub.2022.01.046](https://doi.org/10.1016/j.cub.2022.01.046)
- Jagoda E, Marnetto D, Senevirathne G, Gonzalez V, Baid K, Montinaro F, Richard, D, Falzarano D, LeBlanc EV, Colpitts CC, Banerjee A, Pagani, L, Capellini TD. Regulatory dissection of the severe COVID-19 risk locus introgressed by Neanderthals. Elife. 2023 Feb 10;12:e71235. doi: [10.7554/eLife.71235](https://doi.org/10.1038/s41467-020-15464-w)
- Marnetto D, Pärna K, Läll K, Molinaro L, Montinaro F, Haller T, Metspalu M, Mägi R, Fischer K, Pagani L. Ancestry deconvolution and partial polygenic score can improve susceptibility predictions in recently admixed individuals. Nat Commun. 2020 Apr 2;11(1):1628. doi: [10.1038/s41467-020-15464-w](https://doi.org/10.1038/s41467-020-15464-w)
- Marnetto D, Huerta-Sánchez E. Haplostrips: revealing population structure through haplotype visualization. Methods Ecol Evol. 2017; 8:1389. doi: [10.1111/2041-210X.12747](https://doi.org/10.1111/2041-210X.12747)

</font>
<br clear="left"/>

### Computational methods to infer gene function
<img src="/img/network.png" height="200" align="left" alt="Figure from  et al. 2022" style="padding:10px">
We are interested in computational methods to infer gene function and the phenotypic effects of mutations through the integration of different omics datasets. We develop network analysis methods and machine learning approaches to integrate genomic, transcriptomic, and phenotypic data to understand the role of genetics and gene expression in cancer and diseases.
<font size="2">

**Relevant publications:**
- Lanciano T, Savino A, Porcu F, Cittaro D, Bonchi F, Provero P. Contrast subgraphs allow comparing homogeneous and heterogeneous networks derived from omics data. Gigascience. 2022;12:giad010. doi: [10.1093/gigascience/giad010](https://doi.org/10.1093/gigascience/giad010)
- Draetta EL, Lazarevic D, Provero P, Cittaro D. The frequency of somatic mutations in cancer predicts the phenotypic relevance of germline mutations. Front Genet. 2023 Jan 9;13:1045301. doi: [10.3389/fgene.2022.1045301](https://doi.org/10.3389/fgene.2022.1045301)
- Moiso E, Provero P. Cancer Metabolic Subtypes and Their Association with Molecular and Clinical Features. Cancers (Basel). 2022 Apr 25;14(9):2145. doi: [10.3390/cancers14092145](https://doi.org/10.3390/cancers14092145)
- Savino A, Provero P, Poli V. Differential Co-Expression Analyses Allow the Identification of Critical Signalling Pathways Altered during Tumour Transformation and Progression. Int J Mol Sci. 2020 Dec 12;21(24):9461. doi: [10.3390/ijms21249461](https://doi.org/10.3390/ijms21249461)

</font>
<br clear="left"/>
