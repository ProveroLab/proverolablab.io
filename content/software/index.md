## Software
______

### MatrixRider
[MatrixRider](https://www.bioconductor.org/packages/release/bioc/html/MatrixRider.html) is a Bioconductor package that calculates a single number for a whole sequence that reflects the propensity of a DNA binding protein to interact with it. The DNA binding protein has to be described with a PFM matrix, for example gotten from Jaspar.

### vcf\_rider
[vcf\_rider](https://github.com/vodkatad/vcf_rider)
is a library to efficiently compute sequence-based scores on individual genomes starting from vcf files. vcf\_rider computes the scores only for the number of extant polymorphic sequences and correctly assigns them to different individuals.

### Haplostrips
[Haplostrips](https://bitbucket.org/dmarnetto/haplostrips) is a tool to visualize polymorphisms of a given region of the genome in the form of independently clustered and sorted haplotypes. It is a command-line tool written in Python and R, that uses variant call format files as input and generates a heatmap view.

### Roar
[Roar](https://bioconductor.org/packages/release/bioc/html/roar.html) is a Bioconductor package to identify preferential usage of alternative polyadenylation sites, comparing two biological conditions, starting from known alternative sites and alignments obtained from standard RNA-seq experiments.

### TReX
[TreX](https://githubplus.com/fmarotta/TReX) is a suite of scripts to predict gene expression from the DNA sequence using position weigth matrices, and perform a transcriptome-wide association study (TWAS).

### LAncesTools
[LAncesTools](https://bitbucket.org/dmarnetto/lancestools) is a suite of shell power tools written in Python to deal with Local Ancestry information. With LAncesTools you can convert formats to LAF, sort and extract samples and ancestries, interpolate on different SNPs, apply cut-offs, compute global ancestry proportions, compare local ancestry inferences, etc ...
