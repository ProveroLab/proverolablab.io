This is the website of the Computational Biology Unit led by Prof. Paolo Provero at the University of Torino. We are an interdisciplinary [research team](people) interested in diverse topics regarding computational biology and bioinformatics.

Our [research activity](research) focuses on different aspects of gene regulation. On one hand, we are interested in the evolution and variation of regulatory elements in the human genome. On the other, we are working towards the integration of genetically predicted gene expression in predictive models, especially towards behavioral, neuropsychiatric and neuroimaging-derived complex traits.  
The unit also collaborates with several experimental laboratories providing [bioinformatics](software) and data-mining expertise.

<img src="/img/palazzo.jpg" height="160" align="right" alt="historic anatomy building" style="padding:10px" title="Palazzo degli Istituti Anatomici"> 

Although we started our collaboration already years ago, the CBU recently formed under the Neuroscience Department "Rita Levi Montalcini" and is currently hosted in the historic "Palazzo degli Istituti Anatomici" on [C.so Massimo d'Azeglio 52, Turin](https://goo.gl/maps/xr8szgLNG3e5gNmp9).
<br clear="left"/>

<img src="/img/hike_042024.jpg" alt="Group photo hike" style="padding:10px"> 
<img src="/img/group3.jpg" alt="Group photo" style="padding:10px"> 

<!-- for now disabled, not really working or needed
{{< gallery caption-effect="fade" >}}
  {{< figure link="/img/palazzo.jpg" caption="Palazzo degli Istituti Anatomici" alt="We are currently hosted in the historic Palazzo degli Istituti Anatomici">}}
  {{< figure link="/img/group1.JPG" caption="The group" alt="group photo">}}
  {{< figure src="/img/trex.png" link="research" caption="Research" >}}
  {{< figure src="/img/avatar.png" link="people" caption="People" >}}
  {{< figure src="/img/roar.png" link="software" caption="Software" >}}
{{< /gallery >}}
-->

